package com.devcamp.task6070jparelationshipordercustomer.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task6070jparelationshipordercustomer.model.Customer;

public interface ICustomerRepository extends JpaRepository<Customer, Long>{
    Customer findByid(long id);
}
